package pl.spring.bankApp.model.account;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.spring.bankApp.model.card.Card;
import pl.spring.bankApp.model.user.User;

import java.math.BigDecimal;
import java.util.Random;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.mock;

@DisplayName("Account should")
class AccountTest {

    private final static int ACCOUNT_NUMBER_LENGTH = 12;
    private final static int INITIAL_AMOUNT = 1000;
    private final static int INITIAL_FEE = 0;
    private Account account;

    @BeforeEach
    void init() {
        account = Account.generate();
    }

    @Nested
    @DisplayName("set up card to account correctly")
    class SetUpCard {

        @Test
        @DisplayName("when there is one card")
        void allowAddCard() {
            Card card = Card.generate();
            account.addCard(card);
            Set<Card> cards = account.getCards();
            assertAll(
                    () -> assertNotNull(cards),
                    () -> assertNotNull(card.getAccount()),
                    () -> assertEquals(1, cards.size()),
                    () -> assertTrue(cards.contains(card)),
                    () -> assertEquals(account, card.getAccount())
            );
        }

        @Test
        @DisplayName("when there are multiple cards")
        void allowAddMultipleCards() {
            int randomNumber = new Random().nextInt(10);
            Set<Card> generatedCards = Stream.generate(Card::generate)
                    .limit(randomNumber)
                    .collect(Collectors.toSet());
            generatedCards.forEach(account::addCard);
            assertAll(
                    () -> assertEquals(randomNumber, generatedCards.size()),
                    () -> assertTrue(account.getCards().containsAll(generatedCards)),
                    () -> generatedCards.forEach(
                            card -> assertEquals(account, card.getAccount()))
            );
        }

        @Test
        @DisplayName("when there is null")
        void throwExceptionWhenNullIsGiven() {
            RuntimeException exception = assertThrows(RuntimeException.class, () -> account.addCard(null));
            assertEquals(NullPointerException.class, exception.getClass());
        }
    }

    @Test
    @DisplayName("generate account with expected values correctly")
    void generateAccountCorrectly() {
        assertAll(
                () -> assertEquals(account.getAvailableCash(), new BigDecimal(INITIAL_AMOUNT)),
                () -> assertEquals(account.getFee(), new BigDecimal(INITIAL_FEE))
        );
    }

    @Nested
    @DisplayName("generate account number")
    class AccountNumberTests {

        @Test
        @DisplayName("with correct length")
        void returnLengthForAccountNumber() {
            assertEquals(ACCOUNT_NUMBER_LENGTH, account.getAccountNumber().length());
        }

        @Test
        @DisplayName("only with digits")
        void returnFalseIfContainsOnlyDigits() {
            Pattern pattern = Pattern.compile("\\D");
            Matcher matcher = pattern.matcher(account.getAccountNumber());
            assertFalse(matcher.find());
        }
    }

    @Test
    @DisplayName("subtract given amount correctly")
    void subtractAmountCorrectly() {
        BigDecimal amountToSubtract = new BigDecimal("20.2");
        BigDecimal initialCash = account.getAvailableCash();
        account.subtractAmount(amountToSubtract);
        assertEquals(initialCash.subtract(amountToSubtract), account.getAvailableCash());

    }

    @DisplayName("add given amount correctly")
    @ParameterizedTest
    @MethodSource("data")
    void addAmountCorrectly(BigDecimal givenAmount) {
        BigDecimal initialCash = account.getAvailableCash();
        account.addAmount(givenAmount);
        assertEquals(initialCash.add(givenAmount), account.getAvailableCash());
    }

    static Stream<Arguments> data() {
        return Stream.of(
                Arguments.of(new BigDecimal("100")),
                Arguments.of(new BigDecimal("212.12")),
                Arguments.of(new BigDecimal("0.01"))
        );
    }

    @Test
    @DisplayName("throw exception if account doest'n have enough money")
    void throwExceptionIfNotEnoughMoney() {
        BigDecimal amountToSubtract = new BigDecimal(10000);
        RuntimeException error = assertThrows(RuntimeException.class,
                () -> account.subtractAmount(amountToSubtract));
        assertEquals(String.format("Account with number %s doesn't have enough money" +
                        " to transfer %s", account.getAccountNumber(), amountToSubtract.toString()),
                error.getMessage());
    }

    @Test
    @DisplayName("generate AccountResponse correctly")
    void shouldReturnProperAccountResponse() {
        User user = mock(User.class);
        account.setUser(user);
        AccountResponse accountResponse = account.generateResponse();
        assertAll(
                () -> assertEquals(account.getFee(), accountResponse.getFee()),
                () -> assertEquals(account.getAvailableCash(), accountResponse.getAvailableCash()),
                () -> assertEquals(account.getUser().getId(), accountResponse.getUserId()),
                () -> assertEquals(account.getAccountNumber(), accountResponse.getAccountNumber())
        );
    }
}