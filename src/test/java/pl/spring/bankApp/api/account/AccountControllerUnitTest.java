package pl.spring.bankApp.api.account;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import pl.spring.bankApp.domain.account.AccountFacade;

import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class AccountControllerUnitTest {
    @Autowired
    private MockMvc mockMvc;

    @MockBean
    AccountFacade accountFacade;

    @Test
    void createAccount_Response_Created() throws Exception {
        mockMvc.perform(post("/accounts/users/" + anyLong()))
                .andExpect(status().isCreated());

        verify(accountFacade, times(1)).createAccount(anyLong());
    }

    @Test
    void createAccount_Response_NotFound() throws Exception {
        doThrow(new RuntimeException()).when(accountFacade).createAccount(anyLong());

        mockMvc.perform(post("/accounts/users/" + anyLong()))
                .andExpect(status().isNotFound());

        verify(accountFacade, times(1)).createAccount(anyLong());
    }

    @Test
    void findAllAccounts() throws Exception {
        mockMvc.perform(get("/accounts/"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json("[]"));

        verify(accountFacade, times(1)).findAllAccounts();
    }

    @Test
    void findAccountById_Response_NotFound() throws Exception {
        doThrow(new RuntimeException()).when(accountFacade).findAccountById(anyLong());

        mockMvc.perform(get("/accounts/" + anyLong()))
                .andExpect(status().isNotFound());

        verify(accountFacade, times(1)).findAccountById(anyLong());
    }

    @Test
    void findAccountById_Response_Ok() throws Exception {
        mockMvc.perform(get("/accounts/" + anyLong()))
                .andExpect(status().isOk());

        verify(accountFacade, times(1)).findAccountById(anyLong());
    }

    @Test
    void deleteById_Response_NotFound() throws Exception {
        doThrow(new RuntimeException()).when(accountFacade).deleteAccountById(anyLong());

        mockMvc.perform(delete("/accounts/" + anyLong()))
                .andExpect(status().isNotFound());

        verify(accountFacade, times(1)).deleteAccountById(anyLong());
    }

    @Test
    void deleteById_Response_Ok() throws Exception {
        mockMvc.perform(delete("/accounts/" + anyLong()))
                .andExpect(status().isOk());

        verify(accountFacade, times(1)).deleteAccountById(anyLong());
    }
}