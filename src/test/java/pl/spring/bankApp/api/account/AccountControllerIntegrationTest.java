package pl.spring.bankApp.api.account;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import pl.spring.bankApp.domain.account.DeleteAccountClient;
import pl.spring.bankApp.domain.account.RetrieveAccountClient;
import pl.spring.bankApp.model.account.AccountResponse;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
class AccountControllerIntegrationTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    @SpyBean
    private DeleteAccountClient deleteAccountClientSpy;

    @SpyBean
    private RetrieveAccountClient retrieveAccountClientSpy;

    @Value("${server.servlet.contextPath}")
    private String contextPath;

    private String baseUrl;

    @BeforeEach
    void init() {
        baseUrl = "http://localhost:" + port + contextPath + "/accounts/";
    }

    @DisplayName(value = "Should create account and response code 201")
    @Test
    public void createAccount_ResponseCode_Created() {
        final long userId = 100; // exists in DB
        final String url = this.baseUrl + "users/" + userId;
        int sizeBefore = retrieveAccountClientSpy.findAll().size();

        ResponseEntity<Void> result = this.restTemplate.postForEntity(url, HttpMethod.POST, Void.class);
        int sizeAfter = retrieveAccountClientSpy.findAll().size();

        assertEquals(sizeBefore + 1, sizeAfter);
        assertEquals(201, result.getStatusCodeValue());
    }

    @DisplayName(value = "Should not create account and response code 404")
    @Test
    public void notCreateAccount_ResponseCode_ServerError() {
        final long userId = 10;
        final String url = this.baseUrl + "users/" + userId;
        int sizeBefore = retrieveAccountClientSpy.findAll().size();

        ResponseEntity<Void> result = this.restTemplate.postForEntity(url, HttpMethod.POST, Void.class);
        int sizeAfter = retrieveAccountClientSpy.findAll().size();

        assertEquals(sizeBefore, sizeAfter);
        assertEquals(404, result.getStatusCodeValue());
    }

    @DisplayName(value = "Should not create account and response code 400 due to String")
    @Test
    public void notCreateAccount_ResponseCode_BadRequest_String_Provided() {
        final String userId = "1a";
        final String url = this.baseUrl + "users/" + userId;
        int sizeBefore = retrieveAccountClientSpy.findAll().size();

        ResponseEntity<Void> result = this.restTemplate.postForEntity(url, HttpMethod.POST, Void.class);
        int sizeAfter = retrieveAccountClientSpy.findAll().size();

        assertEquals(sizeBefore, sizeAfter);
        assertEquals(400, result.getStatusCodeValue());
    }

    @Test
    void findAllAccounts_OK_Response() {
        final String url = this.baseUrl;

        ResponseEntity<List> result = this.restTemplate.getForEntity(url, List.class);

        verify(retrieveAccountClientSpy, times(1)).findAll();
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void findAccountById_OK_Response() {
        long accountId = 100; // exists in DB
        final String url = this.baseUrl + accountId;

        ResponseEntity<AccountResponse> result = this.restTemplate.getForEntity(url, AccountResponse.class);

        verify(retrieveAccountClientSpy, times(1)).findById(accountId);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void findAccountById_NotFound_Response() {
        long accountId = 0;
        final String url = this.baseUrl + accountId;

        ResponseEntity<AccountResponse> result = this.restTemplate.getForEntity(url, AccountResponse.class);

        verify(retrieveAccountClientSpy, times(1)).findById(accountId);
        assertEquals(404, result.getStatusCodeValue());
    }

    @Test
    void deleteAccount_OK_Response() {
        long accountId = 100; // exists in DB
        final String url = this.baseUrl + accountId;
        int sizeBefore = retrieveAccountClientSpy.findAll().size();

        ResponseEntity<Void> result = this.restTemplate.exchange(url, HttpMethod.DELETE, HttpEntity.EMPTY, Void.class);
        int sizeAfter = retrieveAccountClientSpy.findAll().size();

        verify(deleteAccountClientSpy, times(1)).deleteAccountById(accountId);
        assertEquals(sizeBefore - 1, sizeAfter);
        assertEquals(200, result.getStatusCodeValue());
    }

    @Test
    void deleteAccount_NotFound_Response() {
        long accountId = 0;
        final String url = this.baseUrl + accountId;
        int sizeBefore = retrieveAccountClientSpy.findAll().size();

        ResponseEntity<Void> result = this.restTemplate.exchange(url, HttpMethod.DELETE, HttpEntity.EMPTY, Void.class);
        int sizeAfter = retrieveAccountClientSpy.findAll().size();

        verify(deleteAccountClientSpy, times(1)).deleteAccountById(accountId);
        assertEquals(sizeBefore, sizeAfter);
        assertEquals(404, result.getStatusCodeValue());
    }
}