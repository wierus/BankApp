INSERT INTO users (id, created_date, last_modified_date, version,
                   first_name, last_name, login, gender)
                   VALUES (100, '22-03-2020', '22-03-2020', 1,
                           'Adam', 'Kowalski', 'user1', 'MALE'),
                           (101, '22-04-2020', '22-04-2020', 1,
                           'Maciej', 'Nowak', 'user3', 'MALE'),
                          (102, '01-04-2010', '01-04-2010', 0,
                           'Marta', 'Sosna', 'user2', 'FEMALE');
INSERT INTO accounts (id, created_date, last_modified_date, version,
                      account_number, available_cash, fee, user_id)
                      VALUES (100, '01-04-2010', '01-04-2010', 0,
                              '120010267891', 1000, 0, 100),
                             (101, '01-04-2010', '01-04-2010', 0,
                              '232100456912', 500, 0, 101),
                             (102, '01-04-2010', '01-04-2010', 0,
                              '345600789128', 1500, 0, 102);
INSERT INTO card (id, cvv, card_number, expiration_date, account_id)
                    VALUES (100, '219', '8423423423420012', '01-04-2025', 102),
                           (101, '986', '8571023923210089', '05-04-2025', 102),
                           (102, '042', '8403232873101022', '01-10-2025', 100);