package pl.spring.bankApp.model.account;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OptimisticLock;
import pl.spring.bankApp.domain.transfer.TransferException;
import pl.spring.bankApp.model.Auditable;
import pl.spring.bankApp.model.card.Card;
import pl.spring.bankApp.model.user.User;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter(value = AccessLevel.PRIVATE)
@Table(name = "ACCOUNTS")
public class Account extends Auditable {

    private final static int ACCOUNT_NUMBER_LENGTH = 12;
    private final static int INITIAL_AMOUNT = 1000;
    private final static int INITIAL_FEE = 0;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "account_sequence")
    @SequenceGenerator(name = "account_sequence")
    private Long id;

    @Column(unique = true)
    private String accountNumber;

    private BigDecimal availableCash;
    private BigDecimal fee;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    @Setter(value = AccessLevel.PUBLIC)
    private User user;

    @OneToMany(mappedBy = "account",
            cascade = CascadeType.ALL, orphanRemoval = true)
    @OptimisticLock(excluded = true)
    private Set<Card> cards = new HashSet<>();

    public void addCard(Card card) {
        cards.add(card);
        card.setAccount(this);
    }

    public static Account generate() {
        Account account = new Account();
        account.setAccountNumber(generateAccountNumber());
        account.setAvailableCash(new BigDecimal(INITIAL_AMOUNT));
        account.setFee(new BigDecimal(INITIAL_FEE));
        return account;
    }

    private static String generateAccountNumber() {
        return UUID.randomUUID()
                .toString().replaceAll("[-|a-z]", "0")
                .substring(0, ACCOUNT_NUMBER_LENGTH);
    }

    public void subtractAmount(BigDecimal amount) {
        BigDecimal newAmount = verifyIfEnoughMoney(amount);
        this.setAvailableCash(newAmount);
    }

    private BigDecimal verifyIfEnoughMoney(BigDecimal amount) {
        BigDecimal newAmount = this.getAvailableCash().subtract(amount);
        if (newAmount.compareTo(BigDecimal.ZERO) < 0) {
            TransferException.notEnoughMoney(this.getAccountNumber(), amount);
        }
        return newAmount;
    }

    public void addAmount(BigDecimal amount) {
        BigDecimal newAmount = this.getAvailableCash().add(amount);
        this.setAvailableCash(newAmount);
    }

    public AccountResponse generateResponse() {
        return AccountMapper.mapToDto(this);
    }

    private static class AccountMapper {
        public static AccountResponse mapToDto(Account account) {
            AccountResponse response = AccountResponse.builder()
                    .accountNumber(account.getAccountNumber())
                    .availableCash(account.getAvailableCash())
                    .fee(account.getFee())
                    .userId(account.getUser().getId())
                    .build();
            return response;
        }
    }
}
