package pl.spring.bankApp.model.account;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;

@Getter
@Builder
public class AccountResponse {

    @NonNull
    private final String accountNumber;

    @NonNull
    private final BigDecimal availableCash;

    @NonNull
    private final BigDecimal fee;

    @NonNull
    private final Long userId;
}
