package pl.spring.bankApp.model.card;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.annotation.Immutable;
import pl.spring.bankApp.model.account.Account;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import java.time.LocalDate;
import java.util.Random;

@Entity
@Immutable
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter(value = AccessLevel.PRIVATE)
public class Card {

    private final static int CVV_LENGTH = 3;
    private final static int CARD_NUMBER_LENGTH = 16;
    private final static int VALIDITY_YEARS = 5;

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "card_sequence")
    @SequenceGenerator(name = "card_sequence")
    private long id;

    @Column(length = CVV_LENGTH)
    private String CVV;

    @Column(length = CARD_NUMBER_LENGTH, unique = true)
    private String cardNumber;

    private LocalDate expirationDate;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "account_id")
    @Setter(value = AccessLevel.PUBLIC)
    private Account account;

    public static Card generate() {
        Card card = new Card();
        card.setCVV(generateNumeric(CVV_LENGTH));
        card.setCardNumber(generateNumeric(CARD_NUMBER_LENGTH));
        card.setExpirationDate(LocalDate.now().plusYears(VALIDITY_YEARS));
        return card;
    }

    private static String generateNumeric(int length) {
        StringBuilder generatedNumeric = new StringBuilder();
        for (int i = 0; i < length; i++) {
            generatedNumeric.append(new Random().nextInt(10));
        }
        return generatedNumeric.toString();
    }

    public CardResponse generateResponse() {
        return CardMapper.mapToDto(this);
    }

    private static class CardMapper {
        private static CardResponse mapToDto(Card card) {
            CardResponse response = CardResponse.builder()
                    .cardNumber(card.getCardNumber())
                    .expirationDate(card.getExpirationDate())
                    .accountId(card.getAccount().getId())
                    .build();
            return response;
        }
    }
}
