package pl.spring.bankApp.model.card;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.time.LocalDate;

@Getter
@Builder
public class CardResponse {

    @NonNull
    private final String cardNumber;

    @NonNull
    private final LocalDate expirationDate;

    private final long accountId;
}
