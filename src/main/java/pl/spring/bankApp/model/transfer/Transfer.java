package pl.spring.bankApp.model.transfer;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.spring.bankApp.model.Auditable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.math.BigDecimal;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Table(name = "TRANSFER")
public class Transfer extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "transfer_sequence")
    @SequenceGenerator(name = "transfer_account")
    @Setter(value = AccessLevel.PRIVATE)
    private Long id;

    @Setter(AccessLevel.PRIVATE)
    private String transferToAccountNumber;

    @Setter(AccessLevel.PRIVATE)
    private String transferFromAccountNumber;

    private BigDecimal amount;

    private String title;

    public static Transfer generate(CreateTransferCommand command, String toAccountNumber) {
        Transfer transfer = new Transfer();
        transfer.setTransferToAccountNumber(toAccountNumber);
        transfer.setTransferFromAccountNumber(command.getClientAccountNumber());
        transfer.setAmount(command.getAmount());
        transfer.setTitle(command.getTitle());
        return transfer;
    }
}
