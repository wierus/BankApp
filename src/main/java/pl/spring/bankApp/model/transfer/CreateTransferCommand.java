package pl.spring.bankApp.model.transfer;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

import java.math.BigDecimal;

@Builder
@Getter
public class CreateTransferCommand {

    private final long ownerId;

    private final long ownerAccountId;

    @NonNull
    private final String clientAccountNumber;

    @NonNull
    private final BigDecimal amount;

    @NonNull
    private final String title;
}
