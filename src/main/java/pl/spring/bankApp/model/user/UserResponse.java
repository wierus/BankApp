package pl.spring.bankApp.model.user;


import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;
import pl.spring.bankApp.model.user.Gender;

@Builder
@Getter
public class UserResponse {

    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @NonNull
    private final String login;

    @NonNull
    private final Gender gender;
}
