package pl.spring.bankApp.model.user;

import lombok.Builder;
import lombok.Getter;
import lombok.NonNull;

@Builder
@Getter
public class CreateUserCommand {

    @NonNull
    private final String firstName;

    @NonNull
    private final String lastName;

    @NonNull
    private final Gender gender;

    @NonNull
    private final String login;
}
