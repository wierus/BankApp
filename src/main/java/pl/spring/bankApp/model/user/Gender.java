package pl.spring.bankApp.model.user;

public enum Gender {
    MALE, FEMALE
}
