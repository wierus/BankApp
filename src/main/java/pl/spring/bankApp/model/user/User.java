package pl.spring.bankApp.model.user;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OptimisticLock;
import pl.spring.bankApp.model.Auditable;
import pl.spring.bankApp.model.account.Account;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import java.util.HashSet;
import java.util.Set;

@Entity
@AllArgsConstructor(access = AccessLevel.PACKAGE)
@NoArgsConstructor
@Getter
@Setter(value = AccessLevel.PRIVATE)
@Table(name = "USERS")
public class User extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "user_sequence")
    @SequenceGenerator(name = "user_sequence")
    private Long id;

    private String firstName;

    private String lastName;

    @Column(unique = true)
    private String login;

    @Enumerated(EnumType.STRING)
    private Gender gender;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    @OptimisticLock(excluded = true)
    private Set<Account> accounts = new HashSet<>();

    public void addAccount(Account account) {
        accounts.add(account);
        account.setUser(this);
    }

    public static User generate(CreateUserCommand createUserCommand) {
        User user = new User();
        user.setFirstName(createUserCommand.getFirstName());
        user.setLastName(createUserCommand.getLastName());
        user.setGender(createUserCommand.getGender());
        user.setLogin(createUserCommand.getLogin());
        return user;
    }

    public UserResponse generateResponse() {
        return UserMapper.mapToDto(this);
    }

    private static class UserMapper {
        private static UserResponse mapToDto(User user) {
            UserResponse userResponse = UserResponse.builder()
                    .firstName(user.getFirstName())
                    .lastName(user.getLastName())
                    .login(user.getLogin())
                    .gender(user.getGender())
                    .build();
            return userResponse;
        }
    }
}
