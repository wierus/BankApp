package pl.spring.bankApp.domain.transfer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.model.transfer.CreateTransferCommand;

import java.util.List;

@Service
@RequiredArgsConstructor
public class TransferFacade {

    private final TransferCreator transferCreator;
    private final RetrievalTransferClient retrievalTransferClient;

    public void createTransfer(CreateTransferCommand createTransferCommand) {
        transferCreator.create(createTransferCommand);
    }

    public void processWaitingTransfers(){
        List<CreateTransferCommand> toProcess = retrievalTransferClient.getPendingTransfers();
        toProcess.forEach(this::createTransfer);
    }
}
