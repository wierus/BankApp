package pl.spring.bankApp.domain.transfer;

import pl.spring.bankApp.model.transfer.CreateTransferCommand;

import java.math.BigDecimal;

public class TransferException extends RuntimeException {

    private TransferException(String message) {
        super(message);
    }

    public static void accountIdAndUserIdNotMatch(CreateTransferCommand createTransferCommand) {
        String message = String.format("UserId %s and AccountId %s doesn't match",
                createTransferCommand.getOwnerId(), createTransferCommand.getOwnerAccountId());
        throw new TransferException(message);
    }

    public static void notEnoughMoney(String accountNumber, BigDecimal amount) {
        String message = String.format("Account with number %s doesn't have enough money to transfer %s",
                accountNumber, amount.toString());
        throw new TransferException(message);
    }
}
