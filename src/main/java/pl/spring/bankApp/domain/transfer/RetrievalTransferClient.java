package pl.spring.bankApp.domain.transfer;

import pl.spring.bankApp.model.transfer.CreateTransferCommand;

import java.util.List;

public interface RetrievalTransferClient {

    List<CreateTransferCommand> getPendingTransfers();
}
