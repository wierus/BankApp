package pl.spring.bankApp.domain.transfer;

import pl.spring.bankApp.model.transfer.Transfer;

public interface CreateTransferClient {

    void create(Transfer transfer);
}
