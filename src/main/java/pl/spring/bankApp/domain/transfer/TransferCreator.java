package pl.spring.bankApp.domain.transfer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.spring.bankApp.domain.account.RetrieveAccountClient;
import pl.spring.bankApp.model.account.Account;
import pl.spring.bankApp.model.transfer.CreateTransferCommand;
import pl.spring.bankApp.model.transfer.Transfer;

import java.util.Optional;

@Service
@RequiredArgsConstructor
@Slf4j
class TransferCreator {

    private final RetrieveAccountClient retrieveAccountClient;
    private final CreateTransferClient createTransferClient;

    @Transactional
    public void create(CreateTransferCommand createTransferCommand) {
        Account toAccount = verifyUserIdAndAccountId(createTransferCommand);
        logInfoOrSubtractMoney(createTransferCommand);
        toAccount.addAmount(createTransferCommand.getAmount());
        Transfer transfer = Transfer.generate(createTransferCommand, toAccount.getAccountNumber());
        createTransferClient.create(transfer);
    }

    private Account verifyUserIdAndAccountId(CreateTransferCommand createTransferCommand) {
        Account account = retrieveAccountClient.findByIdAndUserId(
                createTransferCommand.getOwnerAccountId(),
                createTransferCommand.getOwnerId());
        if (account == null) {
            TransferException.accountIdAndUserIdNotMatch(createTransferCommand);
        }
        return account;
    }

    private void logInfoOrSubtractMoney(CreateTransferCommand createTransferCommand) {
        Optional<Account> result = retrieveAccountClient.findByAccountNumber(
                createTransferCommand.getClientAccountNumber());
        if (!result.isPresent()) {
            log.info("Client account not found. Transaction send to client's bank");
        } else {
            Account fromAccount = result.get();
            fromAccount.subtractAmount(createTransferCommand.getAmount());
        }
    }
}
