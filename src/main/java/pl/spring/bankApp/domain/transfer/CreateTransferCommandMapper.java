package pl.spring.bankApp.domain.transfer;

import pl.spring.bankApp.api.transfer.TransferRequest;
import pl.spring.bankApp.model.transfer.CreateTransferCommand;

public class CreateTransferCommandMapper {

    public static CreateTransferCommand mapperToCreateTransferCommand(TransferRequest transferRequest) {
        return CreateTransferCommand.builder()
                .ownerId(transferRequest.getOwnerId())
                .ownerAccountId(transferRequest.getOwnerAccountId())
                .clientAccountNumber(transferRequest.getClientAccountNumber())
                .amount(transferRequest.getAmount())
                .title(transferRequest.getTitle())
                .build();
    }
}
