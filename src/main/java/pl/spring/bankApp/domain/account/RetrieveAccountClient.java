package pl.spring.bankApp.domain.account;

import pl.spring.bankApp.model.account.Account;

import java.util.List;
import java.util.Optional;

public interface RetrieveAccountClient {

    Optional<Account> findByAccountNumber(String accountNumber);

    Account findByIdAndUserId(Long accountId, Long userId);

    Account findById(Long accountId);

    List<Account> findAll();
}
