package pl.spring.bankApp.domain.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.model.account.Account;
import pl.spring.bankApp.model.account.AccountResponse;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class AccountFacade {

    private final AccountService accountService;
    private final RetrieveAccountClient retrieveAccount;

    public void createAccount(long userId) {
        accountService.createAccount(userId);
    }

    public List<AccountResponse> findAllAccounts() {
        return retrieveAccount.findAll()
                .stream()
                .map(Account::generateResponse)
                .collect(Collectors.toList());
    }

    public AccountResponse findAccountById(long accountId) {
        return retrieveAccount.findById(accountId).generateResponse();
    }

    public void deleteAccountById(long accountId) {
        accountService.deleteAccountById(accountId);
    }
}
