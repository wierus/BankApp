package pl.spring.bankApp.domain.account;

public class AccountException extends RuntimeException {

    private AccountException(String message) {
        super(message);
    }

    public static void accountNotFound(long accountId) {
        throw new AccountException(
                String.format("Account with id=%s not found", accountId));
    }
}
