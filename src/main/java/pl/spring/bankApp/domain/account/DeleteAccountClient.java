package pl.spring.bankApp.domain.account;

public interface DeleteAccountClient {

    void deleteAccountById(long accountId);
}