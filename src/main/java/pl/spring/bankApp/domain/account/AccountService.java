package pl.spring.bankApp.domain.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.spring.bankApp.domain.user.RetrieveUserClient;
import pl.spring.bankApp.domain.user.UserException;
import pl.spring.bankApp.model.account.Account;
import pl.spring.bankApp.model.user.User;

@Service
@RequiredArgsConstructor
class AccountService {

    private final RetrieveUserClient retrieveUserClient;
    private final DeleteAccountClient deleteAccountClient;

    @Transactional
    public void createAccount(long userId) {
        try {
            User user = retrieveUserClient.findById(userId);
            Account account = Account.generate();
            user.addAccount(account);
        } catch (Exception e) {
            UserException.userNotFound(userId);
        }
    }

    @Transactional
    public void deleteAccountById(long accountId) {
        try {
            deleteAccountClient.deleteAccountById(accountId);
        } catch (Exception e) {
            AccountException.accountNotFound(accountId);
        }
    }
}