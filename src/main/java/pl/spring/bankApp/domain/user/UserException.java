package pl.spring.bankApp.domain.user;

public class UserException extends RuntimeException {

    private UserException(String message) {
        super(message);
    }

    public static void userWithLoginAlreadyExist(String login) {
        throw new UserException(
                String.format("User with %s login already exist", login));
    }

    public static void userNotFound(long userId) {
        throw new UserException(
                String.format("User with id=%s not found", userId));
    }
}
