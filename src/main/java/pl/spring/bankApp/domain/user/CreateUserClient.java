package pl.spring.bankApp.domain.user;

import pl.spring.bankApp.model.user.User;

public interface CreateUserClient {

    void create(User user);
}
