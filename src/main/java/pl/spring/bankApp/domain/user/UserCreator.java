package pl.spring.bankApp.domain.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.spring.bankApp.model.user.CreateUserCommand;
import pl.spring.bankApp.model.user.User;

import java.util.Optional;

@RequiredArgsConstructor
@Service
class UserCreator {

    private final CreateUserClient createUserClient;
    private final RetrieveUserClient retrieveUserClient;

    @Transactional
    public void create(CreateUserCommand createUserCommand) {
        Optional<User> result = retrieveUserClient.findByLogin(createUserCommand.getLogin());
        if (result.isPresent()) {
            UserException.userWithLoginAlreadyExist(createUserCommand.getLogin());
        }
        User user = User.generate(createUserCommand);
        createUserClient.create(user);
    }
}
