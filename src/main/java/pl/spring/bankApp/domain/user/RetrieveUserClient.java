package pl.spring.bankApp.domain.user;

import pl.spring.bankApp.model.user.User;

import java.util.List;
import java.util.Optional;

public interface RetrieveUserClient {

    User findById(long id);

    Optional<User> findByLogin(String login);

    List<User> findAll();
}
