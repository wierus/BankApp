package pl.spring.bankApp.domain.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.model.user.CreateUserCommand;
import pl.spring.bankApp.model.user.User;
import pl.spring.bankApp.model.user.UserResponse;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class UserFacade {

    private final UserCreator userCreator;
    private final RetrieveUserClient retrieveUserClient;

    public void createUser(CreateUserCommand createUserCommand) {
        userCreator.create(createUserCommand);
    }

    public UserResponse findUserById(long userId) {
        User user = retrieveUserClient.findById(userId);
        return user.generateResponse();
    }

    public List<UserResponse> findAllUsers() {
        return retrieveUserClient.findAll()
                .stream()
                .map(User::generateResponse)
                .collect(Collectors.toList());
    }
}
