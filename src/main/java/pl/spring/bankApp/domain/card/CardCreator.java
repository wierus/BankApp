package pl.spring.bankApp.domain.card;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.spring.bankApp.domain.account.AccountException;
import pl.spring.bankApp.domain.account.RetrieveAccountClient;
import pl.spring.bankApp.model.account.Account;
import pl.spring.bankApp.model.card.Card;

@Service
@RequiredArgsConstructor
class CardCreator {

    private final RetrieveAccountClient retrieveAccount;

    @Transactional
    public void create(long auctionId) {
        try {
            Account account = retrieveAccount.findById(auctionId);
            Card card = Card.generate();
            account.addCard(card);
        } catch (Exception e) {
            AccountException.accountNotFound(auctionId);
        }
    }
}
