package pl.spring.bankApp.domain.card;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.model.card.Card;
import pl.spring.bankApp.model.card.CardResponse;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class CardFacade {

    private final RetrieveCardClient retrieveCard;
    private final CardCreator cardCreator;

    public List<CardResponse> findAllCards() {
        return retrieveCard.findAll().stream()
                .map(Card::generateResponse)
                .collect(Collectors.toList());
    }

    public void createCard(long auctionId) {
        cardCreator.create(auctionId);
    }

    public CardResponse findCardById(long cardId) {
        return retrieveCard.findById(cardId).generateResponse();
    }
}
