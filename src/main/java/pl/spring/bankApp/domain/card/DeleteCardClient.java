package pl.spring.bankApp.domain.card;

public interface DeleteCardClient {

    void deleteCardById(long cardId);
}