package pl.spring.bankApp.domain.card;

import pl.spring.bankApp.model.card.Card;

import java.util.List;

public interface RetrieveCardClient {

    List<Card> findAll();

    Card findById(long cardId);
}
