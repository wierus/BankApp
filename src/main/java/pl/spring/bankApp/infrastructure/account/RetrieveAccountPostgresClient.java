package pl.spring.bankApp.infrastructure.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.domain.account.RetrieveAccountClient;
import pl.spring.bankApp.model.account.Account;

import java.util.List;
import java.util.Optional;


@Service
@RequiredArgsConstructor
class RetrieveAccountPostgresClient implements RetrieveAccountClient {

    private final AccountRepository accountRepository;

    //TODO czy transfer przyjmuje ujemne kwoty
    //TODO obsługa lub rzucanie wyjątku przez metode orElse
    @Override
    public Optional<Account> findByAccountNumber(String accountNumber) {
        return accountRepository.findByAccountNumber(accountNumber);
    }

    @Override
    public Account findByIdAndUserId(Long accountId, Long userId) {
        return accountRepository.findByIdAndUserId(accountId, userId);
    }

    @Override
    public Account findById(Long accountId) {
        return accountRepository.getOne(accountId);
    }

    @Override
    public List<Account> findAll() {
        return accountRepository.findAll();
    }
}
