package pl.spring.bankApp.infrastructure.account;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.domain.account.DeleteAccountClient;

@Service
@RequiredArgsConstructor
class DeleteAccountPostgresClient implements DeleteAccountClient {

    private final AccountRepository accountRepository;

    @Override
    public void deleteAccountById(long accountId) {
        accountRepository.deleteById(accountId);
    }
}