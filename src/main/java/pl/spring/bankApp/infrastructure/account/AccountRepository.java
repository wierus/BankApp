package pl.spring.bankApp.infrastructure.account;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.spring.bankApp.model.account.Account;

import java.util.Optional;

public interface AccountRepository extends JpaRepository<Account, Long> {

    Optional<Account> findByAccountNumber(String accountNumber);

    Account findByIdAndUserId(Long accountId, Long userId);
}
