package pl.spring.bankApp.infrastructure.transfer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
import pl.spring.bankApp.domain.transfer.CreateTransferClient;
import pl.spring.bankApp.model.transfer.Transfer;

@Service
@RequiredArgsConstructor
@RestController
class CreateTransferPostgresClient implements CreateTransferClient {

    private final TransferRepository transferRepository;

    @Override
    public void create(Transfer transfer) {
        transferRepository.save(transfer);
    }
}
