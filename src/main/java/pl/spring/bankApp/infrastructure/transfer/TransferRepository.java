package pl.spring.bankApp.infrastructure.transfer;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.spring.bankApp.model.transfer.Transfer;

interface TransferRepository extends JpaRepository<Transfer, Long> {
}
