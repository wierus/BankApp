package pl.spring.bankApp.infrastructure.transfer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import pl.spring.bankApp.api.transfer.TransferRequest;
import pl.spring.bankApp.model.transfer.CreateTransferCommand;
import pl.spring.bankApp.domain.transfer.CreateTransferCommandMapper;
import pl.spring.bankApp.domain.transfer.RetrievalTransferClient;

import java.util.List;
import java.util.stream.Collectors;

@Service
class RetrievalTransferRestClient implements RetrievalTransferClient {

    private final RestTemplate restTemplate;
    private final String pendingTransfersUrl;

    @Autowired
    public RetrievalTransferRestClient(RestTemplate restTemplate,
                                       @Value("${pending.transfers.retrieval.auctionapp.url}") String pendingTransfersUrl) {
        this.restTemplate = restTemplate;
        this.pendingTransfersUrl = pendingTransfersUrl;
    }

    @Override
    public List<CreateTransferCommand> getPendingTransfers() {
        List<TransferRequest> result = restTemplate.exchange(pendingTransfersUrl,
                HttpMethod.GET,
                HttpEntity.EMPTY,
                new ParameterizedTypeReference<List<TransferRequest>>() {
                }).getBody();
        return result.stream()
                .map(CreateTransferCommandMapper::mapperToCreateTransferCommand)
                .collect(Collectors.toList());
    }
}
