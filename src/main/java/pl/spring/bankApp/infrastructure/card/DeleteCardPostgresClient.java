package pl.spring.bankApp.infrastructure.card;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.domain.card.DeleteCardClient;

@Service
@RequiredArgsConstructor
class DeleteCardPostgresClient implements DeleteCardClient {

    private final CardRepository cardRepository;

    @Override
    public void deleteCardById(long cardId) {
        cardRepository.deleteById(cardId);
    }
}