package pl.spring.bankApp.infrastructure.card;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.spring.bankApp.model.card.Card;

interface CardRepository extends JpaRepository<Card, Long> {
}
