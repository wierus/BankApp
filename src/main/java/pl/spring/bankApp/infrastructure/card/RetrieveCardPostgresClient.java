package pl.spring.bankApp.infrastructure.card;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.domain.card.RetrieveCardClient;
import pl.spring.bankApp.model.card.Card;

import java.util.List;

@Service
@RequiredArgsConstructor
class RetrieveCardPostgresClient implements RetrieveCardClient {

    private final CardRepository cardRepository;

    @Override
    public List<Card> findAll() {
        return cardRepository.findAll();
    }

    @Override
    public Card findById(long cardId) {
        return cardRepository.getOne(cardId);
    }
}
