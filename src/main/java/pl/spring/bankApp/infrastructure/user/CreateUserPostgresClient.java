package pl.spring.bankApp.infrastructure.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.domain.user.CreateUserClient;
import pl.spring.bankApp.model.user.User;

@Service
@RequiredArgsConstructor
class CreateUserPostgresClient implements CreateUserClient {

    private final UserRepository userRepository;

    @Override
    public void create(User user) {
        userRepository.save(user);
    }
}
