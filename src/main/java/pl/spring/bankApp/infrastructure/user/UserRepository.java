package pl.spring.bankApp.infrastructure.user;

import org.springframework.data.jpa.repository.JpaRepository;
import pl.spring.bankApp.model.user.User;
import java.util.Optional;

interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByLogin(String login);
}
