package pl.spring.bankApp.infrastructure.user;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import pl.spring.bankApp.domain.user.RetrieveUserClient;
import pl.spring.bankApp.model.user.User;

import java.util.List;
import java.util.Optional;

@RequiredArgsConstructor
@Service
class RetrieveUserPostgresClient implements RetrieveUserClient {

    private final UserRepository userRepository;

    @Override
    public User findById(long id) {
        return userRepository.getOne(id);
    }

    @Override
    public Optional<User> findByLogin(String login) {
        return userRepository.findByLogin(login);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }
}
