package pl.spring.bankApp.api;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
class HomeController {

    @Value("${app.version}")
    private String appVersion;

    @GetMapping(path = "/")
    public Map<String, String> getStatus() {
        Map<String, String> map = new HashMap<>();
        map.put("BankApp-version: ", appVersion);
        return map;
    }
}