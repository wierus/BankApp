package pl.spring.bankApp.api.user;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.spring.bankApp.domain.user.UserFacade;
import pl.spring.bankApp.model.user.CreateUserCommand;
import pl.spring.bankApp.model.user.UserResponse;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/users")
@RequiredArgsConstructor
class UserController {

    private final UserFacade userFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@Valid @RequestBody UserRequest userRequest) {
        CreateUserCommand createUserCommand = CreateUserCommand.builder()
                .firstName(userRequest.getFirstName())
                .lastName(userRequest.getLastName())
                .gender(userRequest.getGender())
                .login(userRequest.getLogin())
                .build();
        userFacade.createUser(createUserCommand);
    }

    @GetMapping(path = "/{userId}")
    public ResponseEntity<UserResponse> findById(@PathVariable long userId) {
        try {
            UserResponse userResponse = userFacade.findUserById(userId);
            return ResponseEntity.ok(userResponse);
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<List<UserResponse>> findAll() {
        return ResponseEntity.ok(userFacade.findAllUsers());
    }
}
