package pl.spring.bankApp.api.user;

import lombok.Data;
import pl.spring.bankApp.model.user.Gender;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
class UserRequest {

    @NotNull
    @NotEmpty
    private String firstName;

    @NotNull
    @NotEmpty
    private String lastName;

    @NotNull
    private Gender gender;

    @NotNull
    @NotEmpty
    private String login;
}
