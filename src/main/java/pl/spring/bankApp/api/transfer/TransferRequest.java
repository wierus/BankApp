package pl.spring.bankApp.api.transfer;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonPOJOBuilder;
import lombok.Builder;
import lombok.Value;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

@Value
@Builder
@JsonDeserialize(builder = TransferRequest.TransferRequestBuilder.class)
public class TransferRequest {

    @Min(1)
    private final long ownerId;

    @Min(1)
    private final long ownerAccountId;

    @NotNull
    private final String clientAccountNumber;

    @NotNull
    @JsonProperty("totalPrice")
    private final BigDecimal amount;

    @NotNull
    private final String title;

    @JsonPOJOBuilder(withPrefix = "")
    public static class TransferRequestBuilder {
    }
}
