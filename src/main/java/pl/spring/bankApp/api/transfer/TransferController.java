package pl.spring.bankApp.api.transfer;


import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.spring.bankApp.domain.transfer.CreateTransferCommandMapper;
import pl.spring.bankApp.domain.transfer.TransferFacade;

import javax.validation.Valid;

@RestController
@RequestMapping("/transfers")
@RequiredArgsConstructor
class TransferController {

    private final TransferFacade transferFacade;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public void transfer(@Valid @RequestBody TransferRequest transferRequest){
        transferFacade.createTransfer(
                CreateTransferCommandMapper.mapperToCreateTransferCommand(transferRequest));
    }

    @PostMapping("/process")
    public void processWaitingTransfers(){
        transferFacade.processWaitingTransfers();
    }
}
