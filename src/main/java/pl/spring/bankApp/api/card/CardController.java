package pl.spring.bankApp.api.card;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import pl.spring.bankApp.domain.card.CardFacade;
import pl.spring.bankApp.model.card.CardResponse;

import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/cards")
@RequiredArgsConstructor
class CardController {

    private final CardFacade cardFacade;

    @GetMapping
    public ResponseEntity<List<CardResponse>> findAll() {
        return ResponseEntity.ok(cardFacade.findAllCards());
    }

    @PostMapping(path = "/accounts/{accountId}")
    @ResponseStatus(HttpStatus.CREATED)
    public void create(@PathVariable @Min(1) long accountId) {
        cardFacade.createCard(accountId);
    }

    @GetMapping(path = "/{cardId}")
    public ResponseEntity<CardResponse> findById(@PathVariable @Min(1) long cardId) {
        try {
            return ResponseEntity.ok(cardFacade.findCardById(cardId));
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }
    }
}
