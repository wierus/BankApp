package pl.spring.bankApp.api.account;

import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import pl.spring.bankApp.domain.account.AccountFacade;
import pl.spring.bankApp.model.account.AccountResponse;

import javax.validation.constraints.Min;
import java.util.List;

@RestController
@RequestMapping("/accounts")
@RequiredArgsConstructor
class AccountController {

    private final AccountFacade accountFacade;

    @PostMapping(path = "users/{userId}")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<?> create(@PathVariable @Min(1) long userId) {
        try {
            accountFacade.createAccount(userId);
            return new ResponseEntity<>(HttpStatus.CREATED);
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<List<AccountResponse>> findAll() {
        return ResponseEntity.ok(accountFacade.findAllAccounts());
    }

    @GetMapping(path = "/{accountId}")
    public ResponseEntity<AccountResponse> findById(@Min(1) @PathVariable long accountId) {
        try {
            return ResponseEntity.ok(accountFacade.findAccountById(accountId));
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public ResponseEntity<?> deleteById(@PathVariable long id) {
        try {
            accountFacade.deleteAccountById(id);
            return new ResponseEntity<>(HttpStatus.OK);
        } catch (Exception ex) {
            return ResponseEntity.notFound().build();
        }
    }
}