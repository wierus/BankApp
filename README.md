# BankApp, Java Application
BankApp is a simple application that uses Spring, Hibernate, Postgres and JUnit 5 with RESTful api (**work in progress**).

## Application Setup
App requires setup environment variables such as: 
<ul>
<li>DB_URL - url to database </li>
<li>DB_USERNAME - database username </li>
<li>DB_PASSWORD - database password </li>
<li>DB_INIT_MODE - recommended to use 'NEVER', <br/> but if you would like to populate a database with data from data.sql file, set to 'ALWAYS' </li>
</ul>
You can also easily set these variables up in application.properties<br/>

## Try App out
Application is available for testing on heroku: https://bankapp-ps.herokuapp.com/bank-app/swagger-ui.html#/